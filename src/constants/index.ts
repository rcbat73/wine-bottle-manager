export const PRODUCTTYPE: Record<'name'|'value', string>[] = [
  { name: 'wine', value: 'Still wine'},
  { name: 'sparkling', value: 'Sparkling wine'},
  { name: 'spirit', value: 'Spirit'},
  { name: 'beer', value: 'Beer'},
];

export const VINTAGE: Record<'name'|'value', string>[] = [
  { name: 'nv', value: 'NV'},
  { name: '2022', value: '2022'},
  { name: '2021', value: '2021'},
  { name: '2020', value: '2020'},
];

export const COLLECTIONIDS = {
  londonWine: '63ea0ab90cee11664a669fdc',
  vieVinum: '620623d7b77ba18d7f4f7748',
};

export const DATA_PAGE_SiZE = 10;
