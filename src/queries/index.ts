/**
 * This query returns a list of products from the Bottlebooks API.
 * Use it to fetch a list of products.
 *
 * You can use `fetch` to send this in the body of a POST request to https://api.bottlebooks.com/graphql.
 *
 * @example
 * ```ts
 * async function getProducts(collectionId: string) {
 *   return fetch("https://api.bottlebooks.me/graphql", {
 *     method: "POST",
 *     headers: { "Content-Type": "application/json" },
 *     body: JSON.stringify(getProductsQuery({ collectionId })),
 *   }).then((res) => res.json());
 * }
 * ```
 *
 * @param variables The variables you want to pass to the query.
 * @returns
 */
export function getProductsQuery(variables: {
    collectionId: string;
    first?: number;
    after?: string;
  }) {
    return {
      variables,
      operationName: "ProductListChallenge",
      query: /* GraphQL */ `
        query ProductListChallenge(
          $collectionId: ID!
          $first: Int
          $after: String
        ) {
          collection(collectionId: $collectionId) {
            name
            registeredProducts(first: $first, after: $after) {
              nodes {
                registrationId
                productId
                productVersionId
                producer {
                  name
                }
                product {
                  bottleImage {
                    publicId
                  }
                  regionHierarchy
                  shortName
                  __typename
                  ... on Wine {
                    vintage
                    wineType
                  }
                  ... on Beer {
                    style
                  }
                  ... on Spirit {
                    spiritType
                    spiritSubtype
                  }
                }
              }
              totalCount
              pageInfo {
                count
                hasPreviousPage
                hasNextPage
                startCursor
                endCursor
              }
            }
          }
        }
      `,
    };
  }
  
  /**
   * This query returns a single product from the Bottlebooks API.
   * Use it to fetch a single product.
   */
  
  export function getProductQuery(variables: {
    collectionId: string;
    registrationId: string;
    productId: string;
  }) {
    return {
      variables,
      operationName: "ProductListChallenge_Product",
      query: /* GraphQL */ `
        query ProductListChallenge_Product(
          $collectionId: ID!
          $registrationId: ID!
          $productId: ID!
        ) {
          collection(collectionId: $collectionId) {
            name
            registration(registrationId: $registrationId) {
              __typename
              ... on SingleRegistration {
                registeredProduct(productId: $productId) {
                  registrationId
                  productId
                  productVersionId
                  producer {
                    name
                  }
                  product {
                    bottleImage {
                      publicId
                    }
                    regionHierarchy
                    shortName
                    __typename
                    description
                    ... on Wine {
                      vintage
                      wineType
                      grapeVarieties {
                        varietyName
                        percentage
                      }
                    }
                    ... on Beer {
                      style
                    }
                    ... on Spirit {
                      spiritType
                      spiritSubtype
                    }
                  }
                }
              }
            }
          }
        }
      `,
    };
  }
  