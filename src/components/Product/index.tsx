import {
  useParams,
  Link,
} from 'react-router-dom';

import Spinner from '../Spinner';
import { useFecthProduct } from '../../hooks';
import './index.css';

const Product = () => {
  const { id } = useParams();
  const { loading, product } = useFecthProduct(id);

  if(loading) {
    return <Spinner />;
  }

  return (
    <main className='container'>
      <Link to='/products' className='back-link'>back to search results</Link>
      <div className='product-container'>
        <div className='image-container'>
          <img src={product?.image.src} alt={product?.image.alt} />
        </div>
        <div className='product-info'>
          <p>{product?.producer}</p>
          <p>{product?.name}</p>
          <p className='common'>{product?.region}</p>
          <p className='common'>{product?.grapeVarieties}</p>
        </div>        
      </div>
      <div className='description'>
          <p>Description</p>
          <p>{product?.description}</p>
      </div>
    </main>
  );
};

export default Product;
