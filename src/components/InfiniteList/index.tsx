import React, { useEffect } from 'react';
import { FixedSizeList as List } from "react-window";
import InfiniteLoader from "react-window-infinite-loader";
import AutoSizer from "react-virtualized-auto-sizer";
import { 
  Link,
} from 'react-router-dom';


import { useFecthData } from '../../hooks';
import './index.css';

interface ProductsProps {
  keyword: string;
  productTypes: string;
  vintage: string;
  onProductsLoaded: React.Dispatch<React.SetStateAction<number>>;
}

const InfiniteList = ({ keyword, productTypes, vintage, onProductsLoaded }: ProductsProps) => {
  const { products, loading, loadMore, hasNextPage } = useFecthData(keyword, productTypes, vintage);
  
  const productsAmount = hasNextPage ? products.length + 1 : products.length;
  const loadMoreProducts = loading ? () => {} : loadMore;
  const isProductLoaded = (index: number) => !hasNextPage || index < products.length;
  
  useEffect(() => {
    onProductsLoaded(products?.length || 0);
  }, [products?.length, onProductsLoaded]);

  const Item = ({ index, style }: { index: number; style: any }) => {
    if (!isProductLoaded(index)) {
      return index < products?.length - 1 ? <div style={style}>Loading...</div> : null; 
    }
    const { id, name, producer, region, image } = products[index];

    return (
      <Link to={`/products/${id}`} style={style} className='item-container'>
        <img className='item-image' src={image.src} alt={image.alt} />
        <div className='item-text'>
          <p>{producer}</p>
          <p>{name}</p>
          <p>{region}</p>
        </div>
      </Link>
    );
  };

  return (
    <div className="InfiniteList-list">
      {
        <AutoSizer>
        {({height, width}: Record<'height' | 'width', number>) => (
          <InfiniteLoader
            isItemLoaded={isProductLoaded}
            itemCount={productsAmount}
            loadMoreItems={loadMoreProducts}>
            {({onItemsRendered, ref}) => (
              <List
                height={height}
                itemCount={productsAmount}
                itemSize={80}
                onItemsRendered={onItemsRendered}
                ref={ref}
                width={width}>
                 {Item}
              </List>
            )}
          </InfiniteLoader>
        )}
      </AutoSizer>
      }
    </div>
  );
};

export default InfiniteList;
