import React, { useState } from 'react';

import './index.css';

interface FilterProps {
  title: string;
  items: Record<'name'|'value', string>[];  
  onSelectedItem: React.Dispatch<React.SetStateAction<string>>;
}

const ProductsFilter = React.memo(({ title, items = [], onSelectedItem}: FilterProps) => {
  const [selectedItem, setSelectedItem] = useState('none');

  const onClickHandler = (event: React.MouseEvent<HTMLLIElement>) => {
    const text = event.currentTarget.textContent?.toLowerCase() || '';
    
    const item = items.find((item: Record<"name" | "value", string>) => {
      return item.value.toLowerCase() === text;
    });
    const textFound = item?.name || 'none';
    setSelectedItem(selectedItem === textFound ? 'none' : textFound);
    onSelectedItem(selectedItem === textFound ? 'none' : textFound);
  }

  return (
    <div className='filter-selector'>
      <h2>{title}</h2>
      <ul className='list-container'>
        {
          items.map(({ name, value }) => {            
            return (
              <li key={name} onClick={onClickHandler} className={`filter-item${selectedItem === name ? ' selected' : ''}`}>{value}</li>
            );
          })
        }      
      </ul>
    </div>    
  )
});

export default ProductsFilter;
