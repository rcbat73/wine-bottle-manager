import React, { useState } from 'react';

import InfiniteList from '../InfiniteList';
import ProductsFilter from '../ProductsFilter';
import './index.css';
import { PRODUCTTYPE, VINTAGE } from '../../constants';

const Home = () => {
  const [productTypes, setProductTypes] = useState('none');
  const [vintage, setVintage] = useState('none');
  const [amoutOfProducts, setAmoutOfProducts] = useState(0);
  const [keyword, setKeyword] = useState('');

  const onSubmitHandler = (event: React.MouseEvent<HTMLFormElement>) => {
    event.preventDefault();
    setKeyword(event.currentTarget.keywords.value.trim().toLocaleLowerCase());
    console.log(event.currentTarget.keywords.value);
  };

  return (
    <main className='outer-container'>
      <div className='filters-container'>
        <ProductsFilter title='Product types' items={PRODUCTTYPE} onSelectedItem={setProductTypes} />
        <ProductsFilter title='Vintages' items={VINTAGE} onSelectedItem={setVintage} />
      </div>
      <div className='main-container'>
        <h1 className='title'>
          Showing you <em>{amoutOfProducts}</em> products
        </h1>
        <form className='search-form' onSubmit={onSubmitHandler}>
          <label htmlFor='name' >type producer or product names, or vintage</label>
          <input name='keywords' placeholder='type producer or product names, or vintage' />
          <button>Search</button>
        </form>
        <div className='products-container'>
          <InfiniteList
            keyword={keyword}
            productTypes={productTypes}
            vintage={vintage}
            onProductsLoaded={setAmoutOfProducts}
          />
        </div>
      </div>
    </main>
  );
};

export default Home;
