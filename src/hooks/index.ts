import { useQuery, gql } from "@apollo/client";

import { getProductsQuery, getProductQuery } from '../queries';
import { productsMapper, productMapper, filterProducts } from '../utils';
import { COLLECTIONIDS, DATA_PAGE_SiZE } from '../constants';

const collectionId = COLLECTIONIDS.vieVinum;

const getDataVariables = (endCursor: string) => ({
    collectionId,
    first: DATA_PAGE_SiZE,
    after: endCursor,  
});

export const useFecthData = (keyword: string, productTypes: string, vintage: string) => {
  const query = getProductsQuery({ collectionId }).query;
  const graphqlQuery = gql`${query}`;
  const { data, loading, fetchMore } = useQuery(graphqlQuery, {
    variables: getDataVariables(''),
    notifyOnNetworkStatusChange: true,
  });

  if (loading && !data?.collection?.registeredProducts) {     
    return { loading, products: [] };
  }

  const loadMore = () => {
    return fetchMore({
      query: graphqlQuery,
      variables: getDataVariables(data?.collection?.registeredProducts?.pageInfo?.endCursor),
      updateQuery: (previousResult, { fetchMoreResult }) => {
        const registeredProducts = fetchMoreResult?.collection?.registeredProducts;
        const newProducts = registeredProducts?.nodes;
        const pageInfo = registeredProducts?.pageInfo;

        return newProducts.length
          ? {
              collection: {
                name: previousResult?.collection?.name,
                registeredProducts: {
                  nodes: [...previousResult?.collection?.registeredProducts?.nodes, ...newProducts],
                  totalCount: registeredProducts?.totalCount || 0,
                  pageInfo,
                },
              },
            }
          : previousResult;
      },
    });
  };
  
  const mappedProducts = productsMapper(data);
  const filteredProducts = filterProducts(mappedProducts.products, keyword, productTypes, vintage);

  return {
    products: filteredProducts,
    hasNextPage: mappedProducts?.pageInfo?.hasNextPage,
    loading,
    loadMore,
  };
};

export const useFecthProduct = (id = '') => {
  const idsArr = id.split('-');
  const query = getProductQuery({
    collectionId: '',
    registrationId: '',
    productId: '',
  }).query;
  const graphqlQuery = gql`${query}`;
  
  const { data, loading } = useQuery(graphqlQuery, {
    variables: {
      collectionId,
      registrationId: idsArr[0],
      productId: idsArr[1],
    },
    notifyOnNetworkStatusChange: true,
  });

  const product = data?.collection?.registration?.registeredProduct;
  if (loading && !product) {     
    return { loading, product: null };
  }

  const mappedProduct = productMapper(product);

  return {
    loading,
    product: mappedProduct,
  };
};
