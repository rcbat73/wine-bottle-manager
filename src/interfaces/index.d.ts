interface ProductsData {
  registrationId: string;
  productId: string;
  productVersionId: string;
  producer: {
    name: string;
  };
  product: {
    bottleImage: {
      publicId?: string;
    };
    regionHierarchy: string[];
    shortName: string;
    __typename: string;
    vintage?: string;
    wineType?: string;
    style?: string;
    spiritType?: string;
  }
}

interface PageInfo {
  count: number;
  hasPreviousPage: boolean;
  hasNextPage: boolean;
  startCursor: string;
  endCursor: string;
}

interface RegistaredProducts {
  nodes: ProductsData[];
  totalCount: number;
  pageInfo: PageInfo;
}

interface ResponseData {
  collection: {
    name: string;
    registeredProducts: RegistaredProducts;
  };
}

interface Product {
  id: string;
  name: string;
  image: Record<'src' | 'alt', string>;
  producer: string;
  region: string;
  type: string;
  vintage?: string;
  wineType?: string;
  style?: string;
  spiritType?: string;
}

interface PageInfo {
  count: number;
  hasPreviousPage: boolean;
  hasNextPage: boolean;
  startCursor: string;
  endCursor: string;
}

interface MappedProductsData {
  products: Product[];
  pageInfo: PageInfo;
}

interface Varieties {
  varietyName : string;
  percentage: number;
}

interface ProductData {
  registrationId: string,
  productId: string,
  productVersionId: string,
  producer: {
    name: string;
  },
  product: {
    bottleImage: {
      publicId: string;
    },
    regionHierarchy:string[],
    shortName: string,
    __typename: string,
    description: string | null,
    vintage: string,
    wineType: string,
    grapeVarieties: Varieties[];
  };
}

interface MappedProductData {
  name: string;
  image: Record<'src' | 'alt', string>;
  producer: string;
  region: string;
  grapeVarieties: string;
  description: string | null;
}

interface ProductType {
  [x]: string;
}
