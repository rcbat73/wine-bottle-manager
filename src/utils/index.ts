import { PRODUCTTYPE, VINTAGE } from '../constants';

const getImageUrl = (publicId: string | null, devicePixelRatio: number) => {
  if (!publicId) {
    return 'https://res.cloudinary.com/bottlebooks/image/upload/w_100,h_100,dpr_3,c_pad,g_center,q_auto,f_auto/images/icon_product-placeholder';
  }
  return `https://res.cloudinary.com/bottlebooks/image/upload/w_100,h_100,dpr_${devicePixelRatio},c_pad,g_center,q_auto,f_auto/${publicId}`;
};

export const filterProducts = (
  products: Product[],
  keyword: string,
  productTypes: string,
  vintage: string
): Product[] => {
  const filtered = products.filter((product: Product) => {
    let isVintageSelected = false;
    switch (vintage) {
      case VINTAGE[0].name:
        isVintageSelected = product?.vintage  === VINTAGE[0].value;
        break;
      case VINTAGE[1].name:
        isVintageSelected = product?.vintage  === VINTAGE[1].value;
        break;
      case VINTAGE[2].name:
        isVintageSelected = product?.vintage  === VINTAGE[2].value;
        break;
      case VINTAGE[3].name:
        isVintageSelected = product?.vintage  === VINTAGE[3].value;
        break;
      default:
        isVintageSelected = true;
    }    

    switch (productTypes) {
      case PRODUCTTYPE[0].name:
        return product.wineType === PRODUCTTYPE[0].value && isVintageSelected;
      case PRODUCTTYPE[1].name:
        return product.wineType === PRODUCTTYPE[1].value;
      case PRODUCTTYPE[2].name:
        return product?.spiritType?.toLowerCase() === PRODUCTTYPE[2].value;
      case PRODUCTTYPE[3].name:
        return product?.style?.toLowerCase() === PRODUCTTYPE[3].value;
      default:        
        if(vintage !== 'none') {
          return product?.vintage === vintage;
        }
        return true;
    }
  });
  
  const result = filtered.filter((product) => {
    return product?.producer?.toLowerCase().includes(keyword)
      || product?.name?.toLowerCase().includes(keyword)
      || product?.vintage?.toLowerCase().includes(keyword)
  });

  return result;
};

export const productsMapper = (reponseData: ResponseData): MappedProductsData => {
  const gisteredProducts = reponseData?.collection?.registeredProducts;
  const rawProducts: ProductsData[] = gisteredProducts?.nodes || [];
  const pageInfo = gisteredProducts?.pageInfo;

  const products = rawProducts.map((item) => {
    const product = item?.product;   
    
    return {
      id: `${item?.registrationId}-${item?.productId}`,
      name: product?.shortName,
      image: {
        src: getImageUrl(product?.bottleImage?.publicId || null, window.devicePixelRatio),
        alt: `${product?.shortName}${product?.vintage ? ` ${product?.vintage}` : ''}`,
      },
      producer: item?.producer?.name,
      region: product?.regionHierarchy.join(", "),
      type: product?.__typename,
      vintage: product?.vintage,
      wineType: product?.wineType,
      style: product?.style,
      spiritType: product?.spiritType,
    };

    
  });

  return {
    products,
    pageInfo,
  };
};

export const productMapper = (registeredProduct: ProductData): MappedProductData => {
  const product = registeredProduct?.product;
  const grapeVarieties = product?.grapeVarieties || [];
  return {
    name: product?.shortName,
    image: {
      src: getImageUrl(product?.bottleImage?.publicId || null, window.devicePixelRatio),
      alt: `${product?.shortName}${product?.vintage ? ` ${product?.vintage}` : ''}`,
    },
    producer: registeredProduct?.producer.name,
    region: product?.regionHierarchy?.join(", "),
    grapeVarieties: grapeVarieties?.reduce((acc, item) => `${acc}, ${ item.varietyName }`, ''),
    description: product?.description,
  }
};
