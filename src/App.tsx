import { 
  BrowserRouter as Router,
  Routes,
  Route,
} from 'react-router-dom';
import { ApolloProvider } from '@apollo/client';

import Home from './components/Home';
import Product from './components/Product';
import { client } from './lib/apollo';

const App = () => {
  return (
    <ApolloProvider client={client}>
      <Router>
        <Routes>
          <Route path='/products' element={<Home />} />
          <Route path='/products/:id' element={<Product />} />
          <Route path='*' element={<Home />} />
        </Routes>
      </Router>
    </ApolloProvider>
    
  );
};

export default App;
