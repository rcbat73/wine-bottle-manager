## How to run the app.

Clones the repo:
### `git clone https://gitlab.com/rcbat73/wine-bottle-manager.git`

Go to the project directory, and run:

### `npm install`

After all dependencies are installed, run

### `npm start`

Open [http://localhost:3000](http://localhost:3000) to view the app in a browser.

**As this is a test, I`m sending a file with environment varibles. I don´t do that if I work for a company.**
